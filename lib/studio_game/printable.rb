require_relative 'formatting'
require_relative 'game_stats'

module StudioGame
  module Printable
    include Formatting
    include GameStats

    def print_health_status
      "#{name} (#{health} health)"
    end

    def print_score
      format_dot_spacing(name, score)
    end

    def print_treasure_trove
      treasures = TreasureTrove::TREASURES
      puts "\nThere are #{treasures.size} treasures to be found:"

      treasures.each_with_index do |treasure, number|
        puts "#{number + 1}. #{treasure}"
      end
    end

    def print_player_information
      puts "\nThere are #{players.size} players in #{title}:"

      players.each do |player|
        puts player
      end
    end

    def print_treasures
      puts "\n#{name}'s treasures:"

      found_treasures.each do |name, points|
        puts "#{points} total #{name} points"
      end

      puts "#{name}'s total treasure points: #{points}"
    end
  end
end
