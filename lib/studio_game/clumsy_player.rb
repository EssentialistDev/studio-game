require_relative 'player'
require_relative 'treasure_trove'

module StudioGame
  class ClumsyPlayer < Player
    def find_treasure(treasure = TreasureTrove.random_treasure)
      damaged_treasure = Treasure.new(treasure.name, treasure.points / 2)
      super(damaged_treasure)
    end
  end
end

if __FILE__ == $0
  clumsy = ClumsyPlayer.new("klutz")

  hammer = Treasure.new("hammer", 100)

  puts clumsy.find_treasure(hammer)
  puts clumsy.find_treasure(hammer)
  puts clumsy.find_treasure(hammer)

  crowbar = Treasure.new("crowbar", 400)

  puts clumsy.find_treasure(crowbar)

  puts clumsy.found_treasures
end
