require_relative 'player'
require_relative 'die'
require_relative 'loaded_die'

module StudioGame
  module GameTurn
    def self.take_turn(player)
      die = Die.new
      #die = LoadedDie.new

      puts player.find_treasure

      case die.roll
      when 1..2
        player.w00t
      when 3..4
        puts "#{player.name} was skipped!"
      else
        player.blam
      end
    end
  end
end
