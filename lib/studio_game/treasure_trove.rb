require_relative 'treasure'

module StudioGame
  module TreasureTrove
    TREASURES = [
      Treasure.new("gold", 100),
      Treasure.new("stamp", 30),
      Treasure.new("pickaxe", 2),
      Treasure.new("kindle", 50),
      Treasure.new("ruby", 300),
      Treasure.new("gem", 100)
    ]

    def self.random_treasure
      TREASURES.sample
    end
  end
end

if __FILE__==$0
  puts "#{TreasureTrove::TREASURES.size} Treasures in treasure trove:"
  puts TreasureTrove::TREASURES

  puts TreasureTrove.random_treasure
end
