module StudioGame
  Treasure = Struct.new(:name, :points) do
    def to_s
      "#{name} worth #{points} points"
    end
  end
end

if __FILE__==$0
  gold = Treasure.new("gold", 100)

  puts gold.name
  puts gold.points

  puts gold
end
