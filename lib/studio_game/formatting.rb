module StudioGame
  module Formatting
    def format_dot_spacing(name, attribute_value)
      "#{name.ljust(25, ".")} #{attribute_value}"
    end

    def format_section_title(title)
      "\n" + " #{title.upcase} ".center(20, "-")
    end
  end
end
