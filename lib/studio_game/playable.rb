module StudioGame
  module Playable
    def w00t
      self.health += 15
      puts "#{name} got w00ted and gained 15 health!"
      puts print_health_status
    end

    def blam
      self.health -= 10
      puts "#{name} got blammed and lost 10 health!"
      puts print_health_status
    end

    def strong?
      health >= 100
    end
  end
end
