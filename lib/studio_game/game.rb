require_relative 'player'
require_relative 'game_turn'
require_relative 'treasure_trove'
require_relative 'formatting'
require_relative 'handle_csv'

module StudioGame
  class Game
    include Printable
    include HandleCSV

    attr_reader :title, :players
    def initialize(title = "Game", players = [])
      @title = title.capitalize
      @players = players
    end

    def title=(new_title)
      @title = new_title.capitalize
    end

    def add_player(player)
      @players << player
    end

    def remove_player(player)
      @players.delete(player)
    end

    def play(rounds = 1)
      print_player_information
      print_treasure_trove

      1.upto(rounds) do |round|
        if block_given?
          break if yield
        end

        puts "\nROUND #{round}:"

        @players.each do |player|
          GameTurn.take_turn(player)
        end
      end
    end
  end
end

if __FILE__==$0
  game = Game.new

  # players
  larry = Player.new("larry")
  curly = Player.new("curly")
  moe = Player.new("moe")

  game.add_player(larry)
  game.add_player(curly)
  game.add_player(moe)
  game.remove_player(curly)

  game.play
end
