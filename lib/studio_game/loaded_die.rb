require_relative 'die'

module StudioGame
  class LoadedDie < Die
    def roll
      numbers = [1,1,2,5,6,6]
      @number = numbers.sample
      audit
      @number
    end
  end
end

if __FILE__==$0
  die = LoadedDie.new
  die.number
end
