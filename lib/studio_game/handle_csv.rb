module StudioGame
  module HandleCSV
    def load_players(from_file)
      File.readlines(from_file).each do |line|
        player = Player.from_csv(line)
        add_player(player)
      end
    end

    def save_high_scores(to_file="high_scores.txt")
      File.open(to_file, "w") do |file|
        file.puts "#{self.title} High Scores:"
        self.players.sort.each do |player|
          file.puts player.print_score
        end
      end
    end
  end
end
