module StudioGame
  module GameStats

    def print_stats
      puts format_section_title("game stats")
      print_strength_stats
      print_treasure_stats
      puts total_points
      print_high_scores
    end

    def print_strength_stats

      strong_players, wimpy_players = players.partition { |player| player.strong? }

      puts "\nThere are #{strong_players.size} strong players:"
      strong_players.each do |player|
        puts player.print_health_status
      end

      puts "\nThere are #{wimpy_players.size} wimpy players:"
      wimpy_players.each do |player|
        puts player.print_health_status
      end

    end

    def print_treasure_stats
      players.each do |player|
        player.print_treasures
      end
    end

    def total_points
      players.reduce(0) { |sum, player| sum += player.points }
    end


    def print_high_scores
      puts "\n#{title} High Scores:"
      players.sort.each do |player|
        puts player.print_score
      end
    end

  end
end
