require_relative 'treasure_trove'
require_relative 'playable'
require_relative 'printable'

module StudioGame
  class Player
    include Playable
    include Printable

    attr_accessor :name, :health
    attr_reader   :found_treasures

    def initialize(name = "Player", health = 100)
      @name = name.capitalize
      @health = health
      @found_treasures = Hash.new(0)
    end

    def to_s
      "#{@name}'s health = #{@health} and score = #{score}"
    end

    def name=(new_name)
      @name = new_name.capitalize
    end

    def <=>(other)
      other.score <=> score
    end

    def score
      @health + points
    end

    def points
      @found_treasures.values.sum
    end

    def self.from_csv(string)
      name, health = string.split(',')
      Player.new(name, Integer(health))
    end

    def find_treasure(treasure = TreasureTrove.random_treasure)
      @found_treasures[treasure.name] += treasure.points

      "#{@name} found a #{treasure.name} worth #{treasure.points} points"
    end
  end
end

if __FILE__==$0
  #nameless = Player.new
  larry = Player.new("larry")
  #curly = Player.new("curly", 200)

  puts larry

  puts larry.blam
  puts larry.print_health_status

  puts larry.w00t
  puts larry.print_health_status

  larry.name = "lawrence"
  puts larry

  puts "#{larry.name}'s score is #{larry.score}"

  puts larry.find_treasure
  puts "#{larry.name} has found #{larry.found_treasures.size} treasure"
end
