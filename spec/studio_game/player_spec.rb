require 'studio_game/player'

module StudioGame
  describe "Player should" do

    before do
      # suppress test puts output only
      $stdout = StringIO.new

      # Set friendly test-only variables
      @default_health = 100
      @half_default_health = 50

      # Set up example players
      @larry = Player.new("larry")
      @curly = Player.new("curly")
      @moe = Player.new("moe", @half_default_health)
    end

    it "have a default name of 'Player'." do
      nameless_player = Player.new

      expect(nameless_player.name).to eq("Player")
    end

    it "have a capitalized name." do
      expect(@larry.name).to eq("Larry")
      expect(@curly.name).to eq("Curly")
    end

    it "allow name change requests." do
      @larry.name = "lawrence" #larry's mum is mad
      expect(@larry.name).to eq("Lawrence")
    end

    it "start with a default health of 100." do
      expect(@larry.health).to eq(@default_health)
      expect(@curly.health).to eq(@default_health)
    end

    it "allow a custom starting health." do
      expect(@moe.health).to eq(@half_default_health)
    end

    it "introduce themselves and their health status." do
      expect(@larry.print_health_status).to eq("Larry (100 health)")
      expect(@curly.print_health_status).to eq("Curly (100 health)")
    end

    it "lose 10 health points when blammed." do
      @larry.blam
      expect(@larry.health).to eq(@default_health - 10)

      @larry.blam
      expect(@larry.health).to eq(@default_health - (10*2))
    end

    it "gain 15 health points when w00ted." do
      @larry.w00t
      expect(@larry.health).to eq(@default_health + 15)

      @larry.w00t
      expect(@larry.health).to eq(@default_health + (15*2))
    end

    it "have a score computed from their health and points." do
      treasure = Treasure.new("pickaxe", 2)
      TreasureTrove.stub(:random_treasure).and_return(treasure)
      @larry.find_treasure

      expect(@larry.score).to eq(@default_health + treasure.points)
    end

    it "provide their name and score information." do
      TreasureTrove.stub(:random_treasure).and_return(Treasure.new("pickaxe", 2))
      @larry.find_treasure

      expect(@larry.print_score).to eq("Larry.................... 102")
    end

    it "provide their name, health and score information." do
      TreasureTrove.stub(:random_treasure).and_return(Treasure.new("pickaxe", 2))
      @larry.find_treasure
      @curly.find_treasure
      @moe.find_treasure

      expect(@larry.to_s).to eq("Larry's health = 100 and score = 102")
      expect(@curly.to_s).to eq("Curly's health = 100 and score = 102")
      expect(@moe.to_s).to eq("Moe's health = 50 and score = 52")
    end

    it "be strong if their health is 100." do
      expect(@curly).to be_strong
    end

    it "be strong if their health is 200." do
      @double_initial_health = 200
      @larry = Player.new("larry", @double_initial_health)

      expect(@larry).to be_strong
    end

    it "be wimpy if their health below 100." do
      @half_initial_health = 50
      @one_below_initial_health = 99

      @moe = Player.new("moe", @half_initial_health)
      @curly = Player.new("curly", @one_below_initial_health)

      expect(@moe).not_to be_strong
      expect(@curly).not_to be_strong
    end

    it "find a random treasure" do
      TreasureTrove.stub(:random_treasure).and_return(Treasure.new("ruby", 300))

      expect(@larry.find_treasure).to eq("Larry found a ruby worth 300 points")
    end

    context "accumulate" do
      it "one treasure." do
        TreasureTrove.stub(:random_treasure).and_return(Treasure.new("ruby", 300))

        @larry.find_treasure
        expect(@larry.found_treasures.size).to eq(1)
      end

      it "two treasures." do
        TreasureTrove.stub(:random_treasure).and_return(Treasure.new("ruby", 300), Treasure.new("pickaxe", 2))

        2.times { @larry.find_treasure }
        expect(@larry.found_treasures.size).to eq(2)
      end

      it "three treasures." do
        TreasureTrove.stub(:random_treasure).and_return(Treasure.new("ruby", 300), Treasure.new("pickaxe", 2), Treasure.new("kindle", 50))

        3.times { @larry.find_treasure }
        expect(@larry.found_treasures.size).to eq(3)
      end

      it "two of the same treasures (counts as one treasure found)." do
        TreasureTrove.stub(:random_treasure).and_return(Treasure.new("ruby", 300))

        10.times { @larry.find_treasure }
        expect(@larry.found_treasures.size).to eq(1)
      end
    end

    it "earns points" do
      TreasureTrove.stub(:random_treasure).and_return(Treasure.new("ruby", 300))

      2.times { @larry.find_treasure }
      expect(@larry.points).to eq(600)
    end

    it "can be created from a CSV string" do
      player = Player.from_csv("larry,150")

      expect(player.name).to eq("Larry")
      expect(player.health).to eq(150)
    end

  end
end
