require 'studio_game/treasure'

module StudioGame
  describe "Treasure should" do
    before do
      # suppress test puts output only
      $stdout = StringIO.new

      @fools_gold = Treasure.new("fools gold", 5)
      @bells = Treasure.new("bells", 99)
    end

    it "have a name." do
      expect(@fools_gold.name).to eq("fools gold")
      expect(@bells.name).to eq("bells")
    end

    it "have a point value." do
      expect(@fools_gold.points).to eq(5)
      expect(@bells.points).to eq(99)
    end

    it "share their name and point information." do
      expect(@fools_gold.to_s).to eq("#{@fools_gold.name} worth #{@fools_gold.points} points")
      expect(@bells.to_s).to eq("#{@bells.name} worth #{@bells.points} points")
    end
  end
end
