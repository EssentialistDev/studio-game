require 'studio_game/clumsy_player'

module StudioGame
  describe "ClumsyPlayer should" do
    before do
      # suppress test puts output only
      $stdout = StringIO.new

      @player = ClumsyPlayer.new("klutz")
    end

    it "only get half the point value for each treasure found" do
      hammer = Treasure.new(:hammer, 100)
      half_hammer_points = 50
      half_crowbar_points = 200

      expect(@player.points).to eq(0)

      @player.find_treasure(hammer)
      @player.find_treasure(hammer)
      @player.find_treasure(hammer)

      expect(@player.points).to eq(half_hammer_points * 3)

      crowbar = Treasure.new(:crowbar, 400)
      @player.find_treasure(crowbar)

      expect(@player.points).to eq((half_hammer_points * 3) + (half_crowbar_points))

      expect(@player.found_treasures).to eq({hammer: 150, crowbar: 200})
    end
  end
end
