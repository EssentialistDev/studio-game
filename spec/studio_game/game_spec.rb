require 'studio_game/game'

module StudioGame
  describe "Game should" do

    before do
      # suppress test puts output only
      $stdout = StringIO.new

      @game = Game.new

      @larry = Player.new("larry")
      @curly = Player.new("curly")
      @moe = Player.new("moe")
    end

    it "have a default title of 'Game'." do
      expect(@game.title).to eq("Game")
    end

    it "accept title change requests." do
      @game.title = "scrabble"
      expect(@game.title).to eq("Scrabble")
    end
    it "start with no players." do
      expect(@game.players.size).to eq(0)
    end

    context "add" do
      it "one new player." do
        @game.add_player(@larry)

        first_players_name = @game.players[0].name

        expect(@game.players.size).to eq(1)
        expect(first_players_name).to eq(@larry.name)
      end

      it "two new players." do
        @game.add_player(@larry)
        @game.add_player(@curly)

        second_players_name = @game.players[1].name

        expect(@game.players.size).to eq(2)
        expect(second_players_name).to eq(@curly.name)
      end
    end

    context "remove" do
      before do
        # Setup game so there are already players
        @game.add_player(@larry)
        @game.add_player(@curly)
      end

      it "one player." do
        @game.remove_player(@larry)

        expect(@game.players.size).to eq(1)
      end

      it "two players (in a different order from added)." do
        @game.remove_player(@curly)
        @game.remove_player(@larry)

        expect(@game.players.size).to eq(0)
      end
    end

    context "decide strategy based on a die roll, for example:" do
      before do
        @default_health = 100
        @game.add_player(@larry)
      end

      it "blam player when a high number like 5 or 6 is rolled." do
        Die.any_instance.stub(:roll).and_return(5,6)

        @game.play

        expect(@larry.health).to eq(@default_health - 10)
      end

      it "w00t player when a low number like 1 or 2 is rolled." do
        Die.any_instance.stub(:roll).and_return(1,2)

        @game.play

        expect(@larry.health).to eq(@default_health + 15)
      end

      it "Skip player when a medium number like 3 or 4 is rolled." do
        Die.any_instance.stub(:roll).and_return(3,4)

        @game.play

        expect(@larry.health).to eq(@default_health)
      end

    end

    context "play" do

      before do
        # w00ts the player each round
        Die.any_instance.stub(:roll).and_return(2)

        @default_health = 100

        @game.add_player(@larry)
      end

      it "one round." do
        @game.play

        expect(@larry.health).to eq(@default_health + 15)
      end

      it "two rounds." do
        @game.play(2)

        expect(@larry.health).to eq(@default_health + (15 * 2))
      end

      it "ten rounds." do
        @game.play(10)

        expect(@larry.health).to eq(@default_health + (15 * 10))
      end
    end

    context "sort players" do
      before do
        # Setup initial health values (determines score)
        strongest_starting_health = 300
        strong_starting_health = 200
        wimpy_starting_health = 50

        # Start with several players
        @tintin = Player.new("tintin", strongest_starting_health)
        @gummy_bear = Player.new("gummy bear", strong_starting_health)
        @pickle_rick = Player.new("pickle rick", wimpy_starting_health)

        @players = [@gummy_bear, @pickle_rick, @tintin]
      end

      it "by highest to lowest score." do
        expect(@players.sort).to eq([@tintin, @gummy_bear, @pickle_rick])
      end

    end

    it "keep track of total player points" do
      TreasureTrove.stub(:random_treasure).and_return(Treasure.new("ruby", 300))
      @default_health = 100

      @game.add_player(@larry)
      @game.add_player(@curly)

      @larry.find_treasure
      @curly.find_treasure

      expect(@game.total_points).to eq(300 * 2)
    end

  end
end
