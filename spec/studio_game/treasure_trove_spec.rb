require 'studio_game/treasure_trove'

module StudioGame
  describe "TreasureTrove should" do
    before do
      # suppress test puts output only
      $stdout = StringIO.new

      @treasures = TreasureTrove::TREASURES
    end

    it "have six treasures, including:" do
      expect(@treasures.size).to eq(6)
    end

    context "have treasures already" do
      # Bad practice to test for content, but wanted to try out removing duplication by looping through and this was a good candidate (this is a toy app).

      expected_treasures = {
        "gold":100,
        "stamp":30,
        "pickaxe":2,
        "kindle":50,
        "ruby":300,
        "gem":100
      }

      expected_treasures.each do |expected_name, expected_points|
        it "* A #{expected_name} treasure worth #{expected_points}." do
          expect(@treasures.any? { |actual_treasure|
            actual_treasure.name == expected_name &&
              actual_treasure.points == expected_points
          })
        end
      end

      it "can select a random treasure" do
        treasure = TreasureTrove.random_treasure

        expect(TreasureTrove::TREASURES).to include(treasure)
      end
    end
  end
end
